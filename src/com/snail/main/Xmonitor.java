/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.snail.main;

import com.snail.cfg.XConfig;
import com.snail.cfg.XLogger;
import com.snail.task.CommandTask;
import com.snail.task.HttpTask;
import com.snail.task.MySQLTask;
import com.snail.task.PingTask;
import com.snail.task.ProcessTask;
import com.snail.task.TCPTask;
import it.sauronsoftware.cron4j.Scheduler;
import java.util.TimeZone;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author pm
 */
public class Xmonitor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            TimeoutManager.start();
            Scheduler scheduler = new Scheduler();
            TimeZone.setDefault(TimeZone.getTimeZone(XConfig.getTimezone()));
            TimeZone timeZone = TimeZone.getDefault();
            timeZone.setID(XConfig.getTimezone());
            scheduler.setTimeZone(timeZone);
            scheduler.setDaemon(false);
            for (Element module : XConfig.cfg().select(">modules>module")) {
                String moduleType = module.attr("name");
                String moduleStatus = module.attr("status");
                if ("close".equalsIgnoreCase(moduleStatus)) {
                    continue;
                }
                Elements tasks = module.select(">task");
                for (Element task : tasks) {
                    String taskStatus = task.attr("status");
                    if ("close".equalsIgnoreCase(taskStatus)) {
                        continue;
                    }
                    String cron = task.attr("cron");
                    String taskId;
                    switch (moduleType) {
                        case "http":
                            HttpTask httpTask = new HttpTask(XConfig.cfg(), task, module);
                            taskId = scheduler.schedule(cron, httpTask);
                            httpTask.setTaskId(taskId, scheduler);
                            break;
                        case "command":
                            CommandTask commandTask = new CommandTask(XConfig.cfg(), task, module);
                            taskId = scheduler.schedule(cron, commandTask);
                            commandTask.setTaskId(taskId, scheduler);
                            break;
                        case "process":
                            ProcessTask processTask = new ProcessTask(XConfig.cfg(), task, module);
                            taskId = scheduler.schedule(cron, processTask);
                            processTask.setTaskId(taskId, scheduler);
                            break;
                        case "mysql":
                            MySQLTask mySQLTask = new MySQLTask(XConfig.cfg(), task, module);
                            taskId = scheduler.schedule(cron, mySQLTask);
                            mySQLTask.setTaskId(taskId, scheduler);
                            break;
                        case "tcp":
                            TCPTask tcpTask = new TCPTask(XConfig.cfg(), task, module);
                            taskId = scheduler.schedule(cron, tcpTask);
                            tcpTask.setTaskId(taskId, scheduler);
                            break;
                        case "ping":
                            PingTask pingTask = new PingTask(XConfig.cfg(), task, module);
                            taskId = scheduler.schedule(cron, pingTask);
                            pingTask.setTaskId(taskId, scheduler);
                            break;
                    }
                }
            }

            scheduler.start();
        } catch (Exception e) {
            XLogger.logger.error("Xmonitor Top Exception : ", e);
        }
    }

}
